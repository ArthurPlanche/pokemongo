# Readme

Application ressemblant à pokemonGO fait par Pierre-Alain DURAND et Arthur PLANCHE.

## Fonctionnalités de l'application

### map

- affichage de la map
- récupération des pharmacies pour en faire des pokecenter (besoin d'être à une certaine distance du marker pour soigner ses pokémons en touchant le marker)
- récupération des stations essences pour en faire des shop (besoin d'être à une certaine distance du marker pour obtenir une pokeball et une potion en touchant le marker)
- affichage de pokémon sur la map

### inventaire de l'utilisateur

- Liste des potions et pokeballs de l'utilisateur, si il n'y a qu'un fond vert qui s'affiche cela signifie que l'utilisateur n'a aucun objet.

### Pokedex

- Liste de tous les pokémons existant, lorsque l'on touche sur un pokémon on obtient des informations supplémentaires sur celui-ci.

### Combats pokémons

- Lance un combat entre le pokemon ennemie et le pokemon de l'utilisateur.
- possibilité de réaliser trois actions: 

                                        * Attaquer(attaque unique pour tous les pokemons)
                                        * Se soigner (rend l'ensemble de la vie que si des potions sont a disposition)
                                        * fuir (permet de quitter le combat directement)


### Connexion lors du premier lancement

- Page d'accueil composé d'une zone pour rentrer le nom de l'user et un bouton de connexion mais impossibilité de se connecter.

### Base de donnée

- base de donnée SQLite avec un database handler(possibilité de l'observer avec dbeaver)

### Améliorations

- Capturer les pokemons
- Finir la connexion
- Rendre invisible les pokemons non découvert
- Créer des attaques propres à chaque personnage
- Mettre en place un système d'expérience pour les pokemons

### Phase de test

Afin de tester l'ensemble de nos fonctions, nous avons créé un User User(20,"PA",50) et un pokemon  Pokemon(4,"Salamèche",2131165461,POKEMON_TYPE.Feu,null,0.6,8.5)