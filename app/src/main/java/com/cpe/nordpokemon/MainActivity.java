package com.cpe.nordpokemon;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import android.os.Environment;
import android.util.Log;
import android.view.MenuItem;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.cpe.nordpokemon.databinding.ActivityMainBinding;

import org.osmdroid.config.Configuration;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    private BottomNavigationView bottomNavigationView;
    private ActivityMainBinding binding;
    private final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;
    private DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //for database
        DatabaseHandler db = new DatabaseHandler(this);
        this.db = db;
        db.remplirpokemon(db);
        db.addUser(new User(20,"PA",50));
        db.addPotion(10,"Heal");
        db.addPokeball(10,"Pokeball");
        db.creatuserwithpokemon(new User(20,"PA",50),new Pokemon(4,"Salamèche",2131165461,POKEMON_TYPE.Feu,null,0.6,8.5));
        Log.e("test9",db.getAllPokemonUser(20).get(0).getName());
        //for the map
        Context context = this; //marche aussi avec getApplicationContext();
        Configuration.getInstance().load(context,
                PreferenceManager.
                        getDefaultSharedPreferences(context));
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        requestPermissionsIfNecessary(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.INTERNET,
                Manifest.permission.ACCESS_NETWORK_STATE});

        bottomNavigationView = findViewById(R.id.bottomNavigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        bottomNavigationView.setSelectedItemId(R.id.connexion);
        //showStartup();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int i = 0; i < grantResults.length; i++) {
            if (grantResults[i] == PackageManager.PERMISSION_DENIED ) {
                System.out.println("denied "+permissions[i]);
               this.finish(); //if a permission is denied by the user
            }
            ;
        }

    }

    private void requestPermissionsIfNecessary(String[] permissions) {
        ArrayList<String> permissionsToRequest = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(this, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                permissionsToRequest.add(permission);
            }
        }
        if (permissionsToRequest.size() > 0) {
            ActivityCompat.requestPermissions(
                    this,
                    permissionsToRequest.toArray(new String[0]),
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    public void showStartup() {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        OnClickOnNoteListener listener = new OnClickOnNoteListener(){
            @Override
            public void onClickOnNote(Pokemon pokemon){
                showNoteDetail(pokemon);
            }
        };
        PokedexFragment fragment = new PokedexFragment();
        fragment.setOnClickOnNoteListener(listener);
        transaction.replace(R.id.fragment_container,fragment);
        transaction.commit();

    }

    private void showNoteDetailCombat(Pokemon pokemon) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        CombatFragment fragment = new CombatFragment(db,pokemon);
        transaction.replace(R.id.fragment_container,fragment);
        transaction.addToBackStack("");
        transaction.commit();
    }
    private void showNoteDetail(Pokemon pokemon) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        DetailPokemonFragment fragment = new DetailPokemonFragment(pokemon);
        transaction.replace(R.id.fragment_container,fragment);
        transaction.addToBackStack("");
        transaction.commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.pokemon_list:
                OnClickOnNoteListener listener = new OnClickOnNoteListener(){
                    @Override
                    public void onClickOnNote(Pokemon pokemon){
                        showNoteDetail(pokemon);
                    }
                };
                PokedexFragment fragment = new PokedexFragment();
                fragment.setOnClickOnNoteListener(listener);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
                return true;


            case R.id.maps:
                OnClickOnNoteListener listeners = new OnClickOnNoteListener(){
                    @Override
                    public void onClickOnNote(Pokemon pokemon){
                        showNoteDetailCombat(pokemon);
                    }
                };
                MapsFragment mapsFragment= new MapsFragment(db);
                mapsFragment.setOnClickOnNoteListener(listeners);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, mapsFragment).commit();

                return true;

            case R.id.inventaire:
                InventaireFragment inventaireFragment= new InventaireFragment(db);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, inventaireFragment).commit();
                return true;

            case R.id.connexion:

                ConnectionFragment connectionfragment = new ConnectionFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, connectionfragment).commit();
                return true;

        }
        return false;
    }

}