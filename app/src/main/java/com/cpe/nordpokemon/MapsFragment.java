package com.cpe.nordpokemon;

import static org.osmdroid.views.overlay.Marker.ANCHOR_BOTTOM;
import static org.osmdroid.views.overlay.Marker.ANCHOR_CENTER;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.cpe.nordpokemon.databinding.MapsFragmentBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MapsFragment extends Fragment {
    private MyLocationNewOverlay mLocationOverlay;
    private MapsFragmentBinding binding;
    private Handler handler = new Handler();
    private Runnable runnable;
    static int delay = 7*1000; //Delay for 7 seconds.  One second = 1000 milliseconds.
    private int numberPokemonMap = 0;
    private DatabaseHandler db;
    private OnClickOnNoteListener listener;

    public MapsFragment(DatabaseHandler db){
        this.db = db;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.maps_fragment,container,false);

        Context context = binding.getRoot().getContext();
        Configuration.getInstance().load(context, PreferenceManager.getDefaultSharedPreferences(context));

        MapView map = binding.mapView;
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setMultiTouchControls(true);

        RotationGestureOverlay mRotationGestureOverlay = new RotationGestureOverlay(context, map);
        mRotationGestureOverlay.setEnabled(true);
        map.getOverlays().add(mRotationGestureOverlay);

        this.mLocationOverlay = new MyLocationNewOverlay(new GpsMyLocationProvider(context),map);
        this.mLocationOverlay.enableMyLocation();
        this.mLocationOverlay.enableFollowLocation();
        map.getOverlays().add(this.mLocationOverlay);

        IMapController mapController = map.getController();
        mapController.setZoom(18);

        return binding.getRoot();
        }

    @Override
    public void onPause() {
        super.onPause();
        this.mLocationOverlay.disableFollowLocation();
        this.mLocationOverlay.disableMyLocation();
        if(handler!=null) {
            handler.removeCallbacks(runnable);
        }
        MapView map = binding.mapView;
        map.getOverlays().clear();
        numberPokemonMap = 0;
    }

    @Override
    public void onResume() {
        super.onResume();
        this.mLocationOverlay.enableMyLocation();
        this.mLocationOverlay.enableFollowLocation();

        handler.postDelayed( runnable = new Runnable() {
            public void run() {
                createPokemon();
                threadHeal();
                threadShop();

                handler.postDelayed(runnable, delay);
            }
        }, delay);
    }

    public void threadShop(){
        MapView map = binding.mapView;

        MyThreadEventListener listenerShop = new MyThreadEventListener() {
            @Override
            public void onEventInMyThread(JSONObject object)
            {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                     @Override
                     public void run() {//graphic thread
                         MapView map = binding.mapView;
                         Context context = binding.getRoot().getContext();
                         Configuration.getInstance().load(context, PreferenceManager.getDefaultSharedPreferences(context));
                         Marker marker = new Marker(map);
                         try {
                             marker.setPosition(new GeoPoint(object.getDouble("lat"), object.getDouble("lon")));
                             marker.setIcon(getResources().getDrawable(R.drawable.shop_svgrepo_com));
                             marker.setTitle("Pokeshop " + object.getJSONObject("tags").getString("name"));
                             marker.setOnMarkerClickListener(new Marker.OnMarkerClickListener()
                             {

                                 @Override
                                 public boolean onMarkerClick(Marker marker, MapView mapView) {
                                     Location mylocation = new Location("me");
                                     mylocation.setLongitude(mLocationOverlay.getMyLocation().getLongitude());
                                     mylocation.setLatitude(mLocationOverlay.getMyLocation().getLatitude());

                                     Location markerlocation = new Location("marker");
                                     markerlocation.setLongitude(marker.getPosition().getLongitude());
                                     markerlocation.setLatitude(marker.getPosition().getLatitude());

                                     if (mylocation.distanceTo(markerlocation) < 2000) {
                                         marker.setSubDescription("You've just got a potion and a pokeball !");
                                         User user = db.getUser();

                                         int idPotion = db.getPotionId();
                                         int number_potion = db.getNbPotion(user.getId(),idPotion);
                                         number_potion += 1;
                                         db.updateNbPotion(user.getId(),idPotion,number_potion);

                                         int idPokeball = db.getPokeballId();
                                         int number_pokeball = db.getNbPokeball(user.getId(),idPokeball);
                                         number_pokeball += 1;
                                         db.updateNbPokeball(user.getId(),idPokeball,number_pokeball);

                                         //TODO buy potion with money
                                         marker.showInfoWindow();
                                         return true;
                                     }
                                     marker.showInfoWindow();
                                     return false;
                                 }});
                         } catch (JSONException e) {
                             e.printStackTrace();
                         }
                         marker.setAnchor(ANCHOR_CENTER, ANCHOR_BOTTOM);
                         marker.setSubDescription("It is for shopping");
                         //marker.setIcon(ResourcesCompat.getDrawable(context.getResources(), pokemon.getFrontResource(), context.getTheme()));
                         map.getOverlays().add(marker);
                         map.invalidate();
                     }
                 }
                );
            }
        };
        MyThreadShop threadShop = new MyThreadShop(listenerShop, map.getBoundingBox().getLatSouth(),
                map.getBoundingBox().getLonWest(),
                map.getBoundingBox().getLatNorth(),
                map.getBoundingBox().getLonEast());
        threadShop.start();
    }

    public void threadHeal(){
        MapView map = binding.mapView;

        MyThreadEventListener listenerHeal = new MyThreadEventListener() {
            @Override
            public void onEventInMyThread(JSONObject object)
            {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                     @Override
                     public void run() {//graphic thread
                         MapView map = binding.mapView;
                         Context context = binding.getRoot().getContext();
                         Configuration.getInstance().load(context, PreferenceManager.getDefaultSharedPreferences(context));
                         Marker marker = new Marker(map);
                         try {
                             marker.setPosition(new GeoPoint(object.getDouble("lat"), object.getDouble("lon")));
                             marker.setTitle("PokeCenter " + object.getJSONObject("tags").getString("name"));
                             marker.setIcon(getResources().getDrawable(R.drawable.zhospital_svgrepo_com));
                             marker.setOnMarkerClickListener(new Marker.OnMarkerClickListener()
                             {

                                 @Override
                                 public boolean onMarkerClick(Marker marker, MapView mapView) {
                                     Location mylocation = new Location("me");
                                     mylocation.setLongitude(mLocationOverlay.getMyLocation().getLongitude());
                                     mylocation.setLatitude(mLocationOverlay.getMyLocation().getLatitude());

                                     Location markerlocation = new Location("marker");
                                     markerlocation.setLongitude(marker.getPosition().getLongitude());
                                     markerlocation.setLatitude(marker.getPosition().getLatitude());

                                     if (mylocation.distanceTo(markerlocation) < 2000) {
                                         marker.setSubDescription("Your pokemons have been healed");
                                         User user = db.getUser();
                                         db.HealPokemonsUser(user.getId());
                                         marker.showInfoWindow();
                                         return true;
                                     }
                                     marker.showInfoWindow();
                                     return false;
                                 }});
                         } catch (JSONException e) {
                             e.printStackTrace();
                         }
                         marker.setAnchor(ANCHOR_CENTER, ANCHOR_BOTTOM);
                         marker.setSubDescription("It is for healing pokemons.");
                         //marker.setIcon(ResourcesCompat.getDrawable(context.getResources(), pokemon.getFrontResource(), context.getTheme()));
                         map.getOverlays().add(marker);
                         map.invalidate();
                     }
                 }
                );
            }
        };
        MyThread thread = new MyThread(listenerHeal, map.getBoundingBox().getLatSouth(),
                map.getBoundingBox().getLonWest(),
                map.getBoundingBox().getLatNorth(),
                map.getBoundingBox().getLonEast());
        thread.start();
    }


    public void createPokemon() {
        if (numberPokemonMap < 5) {
            MapView map = binding.mapView;
            Pokemon pokemon = getRandomPokemon();
            Context context = binding.getRoot().getContext();
            Configuration.getInstance().load(context, PreferenceManager.getDefaultSharedPreferences(context));
            Marker marker = new Marker(map);
            marker.setPosition(getRandomGeoPoint());
            marker.setAnchor(ANCHOR_CENTER, ANCHOR_BOTTOM);
            marker.setIcon(ResourcesCompat.getDrawable(context.getResources(), pokemon.getFrontResource(), context.getTheme()));
            marker.setTitle(pokemon.getName());
            String description = "Type: " + pokemon.getType1String();
            if (pokemon.getType2() != null){
                description += " " + pokemon.getType2String();
            }
            description += "<br>Poids: " + pokemon.getWeight() + " kg";
            description += "<br>Taille: " + pokemon.getHeight() + " m";
            marker.setSubDescription(description);
            marker.setOnMarkerClickListener(new Marker.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker, MapView mapView) {
                    onEventFunction(pokemon);
                    return false;
                }
            });


            map.getOverlays().add(marker);
            map.invalidate();
            numberPokemonMap += 1;
        }
    }

    private Pokemon getRandomPokemon(){
        int randomInt = 1 + (int)(Math.random() * ((152-1)+1));

        InputStreamReader isr = new InputStreamReader(getResources().openRawResource(R.raw.poke));
        BufferedReader reader = new BufferedReader(isr);
        StringBuilder builder = new StringBuilder();
        String data = "";

        Pokemon poke = new Pokemon(1,"Bulbizarre",1,POKEMON_TYPE.Plante,POKEMON_TYPE.Poison,0.7,6.9);

        while(data != null) {
            try {
                data = reader.readLine();
                builder.append(data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            JSONArray array = new JSONArray(builder.toString());
            JSONObject object = array.getJSONObject(randomInt - 1);
            String name = object.getString("name");
            String image = object.getString("image");
            int id = getResources().getIdentifier(image,"drawable",
                    binding.getRoot().getContext().getPackageName());
            String type1 = object.getString("type1");
            String type2 = null;
            POKEMON_TYPE type_pokemon_2 = null;
            int order = randomInt;
            if (object.has("type2")) {
                type2 = object.getString("type2");
                type_pokemon_2 = POKEMON_TYPE.valueOf(type2);
            }
            double height = object.getDouble("height");
            double weight = object.getDouble("weight");
            poke = new Pokemon(order,name, id, POKEMON_TYPE.valueOf(type1), type_pokemon_2,height,weight);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return poke;
    }

    private GeoPoint getRandomGeoPoint() {
        double radius = 50;

        double x0 = this.mLocationOverlay.getMyLocation().getLatitude();
        double y0 = this.mLocationOverlay.getMyLocation().getLongitude();

        // Convert radius from meters to degrees
        double radiusInDegrees = radius / 111000f;

        double u = Math.random();
        double v = Math.random();
        double w = radiusInDegrees * Math.sqrt(u);
        double t = 2 * Math.PI * v;
        double x = w * Math.cos(t);
        double y = w * Math.sin(t);

        // Adjust the x-coordinate for the shrinking of the east-west distances
        double new_x = x / Math.cos(y0);

        double foundLatitude = new_x + x0;
        double foundLongitude = y + y0;

        return new GeoPoint(foundLatitude,foundLongitude);
    }

    public void setDBHandler(DatabaseHandler db){
        this.db = db;
    }

    public void onEventFunction(Pokemon pokemon) {
        if (listener != null)
            listener.onClickOnNote(pokemon);
    }

    public void setOnClickOnNoteListener(OnClickOnNoteListener listener)
    {
        this.listener = listener;
    }


}

