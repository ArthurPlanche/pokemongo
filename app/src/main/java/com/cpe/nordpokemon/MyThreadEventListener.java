package com.cpe.nordpokemon;

import org.json.JSONObject;

public interface MyThreadEventListener {
    public void onEventInMyThread(JSONObject object);
}
