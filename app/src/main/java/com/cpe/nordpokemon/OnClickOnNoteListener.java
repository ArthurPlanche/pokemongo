package com.cpe.nordpokemon;

public interface OnClickOnNoteListener{
    public void onClickOnNote(Pokemon pokemon);
}
