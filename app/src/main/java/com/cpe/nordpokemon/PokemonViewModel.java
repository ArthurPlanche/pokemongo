package com.cpe.nordpokemon;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class PokemonViewModel extends BaseObservable {
    private Pokemon pokemon = new Pokemon();




    public void setPokemon(Pokemon pokemon) {
        this.pokemon = pokemon;
        notifyChange();
    }
    @Bindable
    public int getFront() {
        return pokemon.getFrontResource();
    }
    @Bindable
    public String getName() {
        return pokemon.getName();
    }
    @Bindable
    public String getType1() {
        return pokemon.getType1String();
    }
    @Bindable
    public String getWeight() {
        return pokemon.getWeight()+ " kg" ;
    }
    @Bindable
    public String getHeight() {
        return pokemon.getHeight()+ " m" ;
    }
    @Bindable
    public String getType2() {
        if (pokemon.getType2() != null)
            return pokemon.getType2String();
        return "";
    }
    @Bindable
    public int getHP() {
        return pokemon.getHP();
    }

    @Bindable
    public String getNumber() {
        return "#"+pokemon.getOrder();
    }
    public Drawable getImage(Context context, int res) {
        if(res != -1)
            return ResourcesCompat.getDrawable(context.getResources(),
                    res, context.getTheme());
        else
            return null;
    }

    public Pokemon getPokemon() {
        return pokemon;
    }
}
