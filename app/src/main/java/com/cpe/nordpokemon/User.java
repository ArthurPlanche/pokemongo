package com.cpe.nordpokemon;

public class User {

    private Integer id;
    private String name;
    private int argent;

    public User(Integer id, String name , int argent){
        this.id = id;
        this.argent = argent;
        this.name = name;
    }




    public int getArgent() {
        return argent;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setArgent(int argent) {
        this.argent = argent;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
