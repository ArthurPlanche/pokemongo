package com.cpe.nordpokemon;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;


import com.cpe.nordpokemon.databinding.CombatFragmentBinding;

import java.util.ArrayList;
import java.util.List;

public class CombatFragment extends Fragment {

    private final Pokemon ennemy;
    //private final User player;
    private Pokemon ally;
    private PokemonViewModel allyViewModel;
    private PokemonViewModel ennemyViewModel;
    private DatabaseHandler db;



    //Les widgets du xml
    private View panelChoix;


    private Button buttonAttaque;
    private Button buttonSoigner;

    private Button buttonFuite;
    private Button buttonPokeball;

    private View panelRetour;



    private ProgressBar firstToMoveProgressBar;
    private ProgressBar secondToMoveProgressBar;


    private String etatCombat;

    private OnClickOnNoteListener listener;
    private Pokemon firstToMove;
    private Pokemon secondToMove;

    private View firstToMoveView;
    private View secondToMoveView;

    private PokemonViewModel firstToMoveModel;
    private PokemonViewModel secondToMoveModel;
    private CombatFragmentBinding binding;


    public CombatFragment(DatabaseHandler db,Pokemon ennemy) {
        this.db = db;
        this.ennemy = ennemy;
        this.allyViewModel = new PokemonViewModel();
        this.ennemyViewModel = new PokemonViewModel();
        ennemyViewModel.setPokemon(ennemy);
        allyViewModel.setPokemon(db.getAllPokemonUser(20).get(0));


    }

    @Nullable
    @Override

    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        this.binding = DataBindingUtil.inflate(inflater,
            R.layout.combat_fragment, container, false);
        this.binding.setPokemonEnemyViewModel(ennemyViewModel);
        this.binding.setPokemonPlayerViewModel(allyViewModel);
        this.binding.setCombatMessage(String.format("Un combat est lancé contre %s", ennemy.getName()));

        panelChoix = binding.getRoot().findViewById(R.id.panelDecision);



        buttonAttaque = binding.getRoot().findViewById(R.id.button_attaque);
        buttonAttaque.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                disablePanelChoix();
                proceedCombat("attaque");
            }
        });

        buttonSoigner = binding.getRoot().findViewById(R.id.button_soigner);
        buttonSoigner.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                disablePanelChoix();
                proceedCombat("soigner");
            }
        });

        buttonPokeball = binding.getRoot().findViewById(R.id.button_pokeball);
        buttonPokeball.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                disablePanelChoix();
                proceedCombat("pokeball");
            }
        });




        buttonFuite = binding.getRoot().findViewById(R.id.button_fuite);
        buttonFuite.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                disablePanelChoix();
                proceedCombat("fuite");

            }
        });


        animDebutDeCombat();
        return binding.getRoot();

    }

    private void disablePanelChoix() {
        ViewGroup vg = (ViewGroup) panelChoix;
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(false);

        }
    }

    private void enablePanelChoix() {
        ViewGroup vg = (ViewGroup) panelChoix;
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(true);

        }
    }

    private void proceedCombat(String instruction) {

        this.etatCombat = "enCours";
        Pokemon playerPokemon = db.getAllPokemonUser(20).get(0);

        switch (instruction) {
            case ("fuite"):
                this.etatCombat = "fuite";
                endTour();
                break;
            case ("attaque"):

                firstToMove = playerPokemon;
                firstToMoveView = binding.getRoot().findViewById(R.id.ally);
                firstToMoveModel = this.allyViewModel;
                firstToMoveProgressBar = binding.getRoot().findViewById(R.id.allyBar);


                secondToMove = ennemy;
                secondToMoveView = binding.getRoot().findViewById(R.id.ennemy);
                secondToMoveModel = this.ennemyViewModel;
                secondToMoveProgressBar = binding.enemyBar;
                animAttaque1();
                break;
            case ("soigner"):
                //Le pokemon se soigne forcément d'abord
                firstToMove = playerPokemon;
                firstToMoveView = binding.getRoot().findViewById(R.id.ally);
                firstToMoveModel = this.allyViewModel;
                firstToMoveProgressBar = binding.getRoot().findViewById(R.id.allyBar);


                secondToMove = ennemy;
                secondToMoveView = binding.getRoot().findViewById(R.id.ennemy);
                secondToMoveModel = this.ennemyViewModel;
                secondToMoveProgressBar = binding.getRoot().findViewById(R.id.enemyBar);

                healAnim();
                break;


        }

    }

    private void endTour() {
        Pokemon playerPokemon = db.getAllPokemonUser(20).get(0);
        switch (etatCombat) {

            case "fuite":
                binding.setCombatMessage("Vous prenez la fuite");
                panelRetour.setVisibility(View.VISIBLE);
                panelChoix.setVisibility(View.GONE);
                break;

            case "pokemonCatched":
                binding.setCombatMessage("le pokemon est capturé ! ");
                panelRetour.setVisibility(View.VISIBLE);
                panelChoix.setVisibility(View.GONE);
                break;

            case "pokemonDead":
                if (playerPokemon.getHP() == 0) {
                    binding.setCombatMessage("Votre pokemon est mort");

                    Log.d("Combat", "votre pokemon est mort");
                } else {
                    binding.setCombatMessage("L'ennemi est vaincu ! ");
                    Log.d("Combat", "L'ennemi est vaincu ! ");
                }
                panelRetour.setVisibility(View.VISIBLE);
                panelChoix.setVisibility(View.GONE);
                break;

            case "enCours":
                binding.setCombatMessage("Choisissez quoi faire");
                enablePanelChoix();
                break;
        }

    }

    private void animDebutDeCombat() {

        binding.getRoot().findViewById(R.id.allyBar).setVisibility(View.INVISIBLE);
        binding.getRoot().findViewById(R.id.enemyBar).setVisibility(View.INVISIBLE);
        disablePanelChoix();
        ScaleAnimation scaleAnim1 = new ScaleAnimation(0.f, 1.f, 0.f, 1.0f);
        scaleAnim1.setDuration(3000);

        scaleAnim1.setAnimationListener(new Animation.AnimationListener() {


            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                binding.getRoot().findViewById(R.id.allyBar).setVisibility(View.VISIBLE);
                binding.getRoot().findViewById(R.id.enemyBar).setVisibility(View.VISIBLE);
                enablePanelChoix();
                binding.setCombatMessage("Choisissez quoi faire");

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        binding.getRoot().findViewById(R.id.ally).startAnimation(scaleAnim1);
        binding.getRoot().findViewById(R.id.ennemy).startAnimation(scaleAnim1);

    }

    private void animAttaque1() {

        Log.d("x1", String.valueOf(firstToMoveView.getX()));
        Log.d("Y1", String.valueOf(firstToMoveView.getY()));
        Log.d("x2", String.valueOf(secondToMoveView.getX()));
        Log.d("Y2", String.valueOf(secondToMoveView.getY()));


        binding.setCombatMessage(String.format("%s attaque", firstToMove.getName()));
        secondToMove.isAttacked(firstToMove);
        firstToMoveView.setZ(firstToMoveView.getZ() + 1);
        TranslateAnimation pokemonAttack1 = new TranslateAnimation(
            Animation.ABSOLUTE, 0, Animation.ABSOLUTE, secondToMoveView.getX() - firstToMoveView.getX(),
            Animation.ABSOLUTE, 0, Animation.ABSOLUTE, secondToMoveView.getY() - firstToMoveView.getY() * 0.8f);
        pokemonAttack1.setDuration(2000);

        TranslateAnimation pokemonAttack2 = new TranslateAnimation(
            Animation.ABSOLUTE, 0, Animation.ABSOLUTE, -(secondToMoveView.getX() - firstToMoveView.getX()),
            Animation.ABSOLUTE, 0, Animation.ABSOLUTE, -(secondToMoveView.getY() - firstToMoveView.getY() * 0.8f));
        pokemonAttack2.setDuration(2000);
        pokemonAttack2.setStartOffset(2000);

        pokemonAttack2.setAnimationListener(new Animation.AnimationListener() {


            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                firstToMoveView.setZ(firstToMoveView.getZ() - 1);
                animationDegat1();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        AnimationSet as = new AnimationSet(true);
        as.addAnimation(pokemonAttack1);
        as.addAnimation(pokemonAttack2);
        firstToMoveView.startAnimation(as);

    }

    private void animAttaque2() {


        if (secondToMove.getHP() != 0) {

            binding.setCombatMessage(String.format("%s attaque", secondToMove.getName()));
            firstToMove.isAttacked(secondToMove);

            secondToMoveView.setZ(secondToMoveView.getZ() + 1);
            TranslateAnimation pokemonAttack1 = new TranslateAnimation(
                Animation.ABSOLUTE, 0, Animation.ABSOLUTE, firstToMoveView.getX() - secondToMoveView.getX(),
                Animation.ABSOLUTE, 0, Animation.ABSOLUTE, firstToMoveView.getY() - secondToMoveView.getY() * 0.7f);
            pokemonAttack1.setDuration(2000);

            TranslateAnimation pokemonAttack2 = new TranslateAnimation(
                Animation.ABSOLUTE, 0, Animation.ABSOLUTE, -(firstToMoveView.getX() - secondToMoveView.getX()),
                Animation.ABSOLUTE, 0, Animation.ABSOLUTE, -(firstToMoveView.getY() - secondToMoveView.getY() * 0.7f));
            pokemonAttack2.setDuration(2000);
            pokemonAttack2.setStartOffset(2000);

            pokemonAttack2.setAnimationListener(new Animation.AnimationListener() {


                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    secondToMoveView.setZ(secondToMoveView.getZ() - 1);
                    animationDegat2();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            AnimationSet as = new AnimationSet(true);
            as.addAnimation(pokemonAttack1);
            as.addAnimation(pokemonAttack2);
            secondToMoveView.startAnimation(as);

        } else {
            etatCombat = "pokemonDead";
            endTour();
        }

    }

    private void animationDegat1() {

        AlphaAnimation flashAnim1 = new AlphaAnimation(1.f, 0.f);
        flashAnim1.setDuration(200);
        flashAnim1.setRepeatCount(3);

//        ProgressBarAnimation anim = new ProgressBarAnimation(progress, from, to);
//        anim.setDuration(1000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            secondToMoveProgressBar.setProgress(secondToMove.getHP(), true);
        }


        flashAnim1.setAnimationListener(new Animation.AnimationListener() {


            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animAttaque2();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        secondToMoveView.startAnimation(flashAnim1);

    }

    private void animationDegat2() {

        AlphaAnimation flashAnim1 = new AlphaAnimation(1.f, 0.f);
        flashAnim1.setDuration(200);
        flashAnim1.setRepeatCount(3);

//        ProgressBarAnimation anim = new ProgressBarAnimation(progress, from, to);
//        anim.setDuration(1000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            firstToMoveProgressBar.setProgress(firstToMove.getHP(), true);
        }


        flashAnim1.setAnimationListener(new Animation.AnimationListener() {


            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (firstToMove.getHP() == 0) {
                    etatCombat = "pokemonDead";
                }
                endTour();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        firstToMoveView.startAnimation(flashAnim1);

    }

    private void healAnim() {

        if(db.getNbPotion(20,db.getNbPotion(20,db.getPotionId())) > 1 ){
            binding.setCombatMessage(String.format("%s se soigne", firstToMove.getName()));
            firstToMove.getHP();
            int idPotion = db.getPotionId();
            User user = db.getUser();
            int number_potion = db.getNbPotion(user.getId(),idPotion);
            number_potion -= 1;
            db.updateNbPotion(user.getId(),idPotion,number_potion);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                firstToMoveProgressBar.setProgress(firstToMove.getHP(), true);
            }

            ScaleAnimation scaleAnim1 = new ScaleAnimation(1.f, 1.1f, 1.f, 1.1f);
            scaleAnim1.setDuration(500);
            scaleAnim1.setRepeatCount(3);
            scaleAnim1.setAnimationListener(new Animation.AnimationListener() {


                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    animAttaque2();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            firstToMoveView.startAnimation(scaleAnim1);
        }
        else{
            binding.setCombatMessage(String.format("%s plus de potion", firstToMove.getName()));
            ScaleAnimation scaleAnim1 = new ScaleAnimation(1.f, 1.1f, 1.f, 1.1f);
            scaleAnim1.setDuration(500);
            scaleAnim1.setRepeatCount(3);
            scaleAnim1.setAnimationListener(new Animation.AnimationListener() {


                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    animAttaque2();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            firstToMoveView.startAnimation(scaleAnim1);

        }

    }



}


    /*public void setFinDeCombatListener(OnFinDeCombatListener finDeCombatListener) {
        this.finDeCombatListener=finDeCombatListener;
    }*/