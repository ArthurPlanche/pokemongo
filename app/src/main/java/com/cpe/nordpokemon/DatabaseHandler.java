package com.cpe.nordpokemon;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class DatabaseHandler extends SQLiteOpenHelper {
    //information database
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_name = "NORD POKEMON";
    //table user
    private static final String TABLE_USER = "Users";
    private static final String UID = "_id";
    private static final String NAME = "Name";
    private static final String MONEY = "Money";
    // table pokemon
    private static final String TABLE_POKEMON= "POKEMON";
    private static final String UID_POKEMON = "_idPOKEMON";
    private static final String HEIGHT = "Height";
    private static final String WEIGHT = "Weight";
    private static final String FRONTRESSOURCE ="FrontRessource";
    private static final String ID_TYPE1 = "_idType1";
    private static final String ID_TYPE2 = "_idType2";
    private static final String TYPE1 = "Type1";
    private static final String TYPE2 = "Type2";
    private static final String SORT = "Sort";
    private static final String LIFEMAX = "LifeMax";
    //table pokemon user
    private static final String TABLE_USER_POKEMON = "USER_POKEMON";
    private static final String ID_USER = "id_user";
    private static final String ID_POKEMON = "id_pokemon";
    private static final String LIFE = "Life";
    //table pokemon type
    private static final String TABLE_TYPE = "TYPE_POKEMON";
    private static final String ID_TYPE = "_idType";
    //table inventaire
    private static final String TABLE_INVENTAIRE = "INVENTAIRE";
    private static final String POKEBALL = "Pokeball";
    private static final String POTION = "Potion";
    private static final String ID_INVENTAIRE = "_id_inventaire";
    // table potion
    private static final String TABLE_POTION = "POTION";
    private static final String ID_POTION ="_idPotion";
    private static final String NAMEPOTION ="NamePotion";
    //table potion_inventaire
    private static final String TABLE_POTION_INVENTAIRE = "POTION_INVENTAIRE";
    private static final String NUMBER = "Nombre";
    //table pokeball
    private static final String TABLE_POKEBALL = "POKEBALL";
    private static final String ID_POKEBALL ="_idPokeball";
    private static final String NAMEPOKEBALL ="NamePokeball";
    //table pokeball_inventaire
    private static final String TABLE_POKEBALL_INVENTAIRE = "POKEBALL_INVENTAIRE";

    private Context context;


    public DatabaseHandler(Context context){
        super(context,DATABASE_name,null,DATABASE_VERSION);
        this.context= context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_POKEMON);
        db.execSQL("PRAGMA foreign_keys=ON;");

        String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + " ("
                + UID + " INTEGER PRIMARY KEY,"
                + NAME +" VARCHAR(255),"
                + MONEY +" INTEGER);"
                ;
        db.execSQL(CREATE_USER_TABLE);

        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_POKEMON);
        String CREATE_POKEMON_TABLE = "CREATE TABLE " + TABLE_POKEMON+ "("
                + UID_POKEMON + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + NAME +" VARCHAR(255),"
                + HEIGHT+ " double,"
                + WEIGHT + " double,"
                + FRONTRESSOURCE + " int,"
                + ID_TYPE1 + " int,"
                + ID_TYPE2 + " int,"
                + TYPE1 + " VARCHAR(255),"
                + TYPE2 + " VARCHAR(255),"
                + SORT+ " VARCHAR(255),"
                + LIFEMAX + " int,"
                + " FOREIGN KEY ("+ID_TYPE1+") REFERENCES "+TABLE_TYPE+"("+ID_TYPE+"), "
                + " FOREIGN KEY ("+ID_TYPE2+") REFERENCES "+TABLE_TYPE+"("+ID_TYPE+"));"
                ;
        db.execSQL(CREATE_POKEMON_TABLE);
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_USER_POKEMON);

        db.execSQL("DROP TABLE IF EXISTS " +TABLE_USER_POKEMON);
        String CREATE_USER_POKEMON_TABLE = "CREATE TABLE " + TABLE_USER_POKEMON + " ("
                + ID_USER + " INTEGER,"
                + ID_POKEMON + " INTEGER,"
                + LIFE + " int,"
                + " FOREIGN KEY ("+ID_USER+") REFERENCES "+TABLE_USER+"("+UID+"), "
                + " FOREIGN KEY ("+ID_POKEMON+") REFERENCES "+TABLE_POKEMON+"("+UID_POKEMON+"));"
                ;
        db.execSQL(CREATE_USER_POKEMON_TABLE);
        db.execSQL(
                "CREATE UNIQUE INDEX idx_user_pokemon ON "+TABLE_USER_POKEMON+"("+ID_USER+","+ID_POKEMON+") ;"
        );

        String CREATE_TYPE_TABLE = "CREATE TABLE " + TABLE_TYPE + " ("
                + ID_TYPE + " INTEGER PRIMARY KEY,"
                + NAME + " VARCHAR(255));"
                ;
        db.execSQL(CREATE_TYPE_TABLE);

        String CREATE_INVENTAIRE_TABLE = "CREATE TABLE " + TABLE_INVENTAIRE + " ("
                + ID_USER + " INTEGER,"
                + POKEBALL + " VARCHAR(255),"
                + POTION + " VARCHAR(255),"
                + " FOREIGN KEY ("+ID_USER+") REFERENCES "+TABLE_USER+"("+ID_USER+"));"
                ;
        db.execSQL(CREATE_INVENTAIRE_TABLE);

        String CREATE_POTION_TABLE = "CREATE TABLE " + TABLE_POTION + " ("
                +ID_POTION+ " INTEGER PRIMARY KEY,"
                +NAMEPOTION+ " VARCHAR(255));"
                ;
        db.execSQL(CREATE_POTION_TABLE);

        String CREATE_POTION_INVENTAIRE_TABLE = "CREATE TABLE " + TABLE_POTION_INVENTAIRE + " ("
                + ID_POTION +" INTEGER,"
                + ID_USER + " INTEGER,"
                + NUMBER + " int,"
                + " FOREIGN KEY ("+ID_USER+") REFERENCES "+TABLE_INVENTAIRE+"("+ID_USER+"), "
                + " FOREIGN KEY ("+ID_POTION+") REFERENCES "+TABLE_POTION+"("+ID_POTION+"),"
                + "CONSTRAINT uPotionUser UNIQUE (" + ID_USER + "," + ID_POTION+ "));"
                ;
        db.execSQL(CREATE_POTION_INVENTAIRE_TABLE);


        String CREATE_POKEBALL_TABLE = "CREATE TABLE " + TABLE_POKEBALL + " ("
                +ID_POKEBALL+ " INTEGER PRIMARY KEY,"
                +NAMEPOKEBALL+ " VARCHAR(255));"
                ;
        db.execSQL(CREATE_POKEBALL_TABLE);

        String CREATE_POKEBALL_INVENTAIRE_TABLE = "CREATE TABLE " + TABLE_POKEBALL_INVENTAIRE + " ("
                + ID_POKEBALL +" INTEGER,"
                + ID_USER + " INTEGER,"
                + NUMBER + " int,"
                + " FOREIGN KEY ("+ID_USER+") REFERENCES "+TABLE_INVENTAIRE+"("+ID_USER+"), "
                + " FOREIGN KEY ("+ID_POKEBALL+") REFERENCES "+TABLE_POKEBALL+"("+ID_POKEBALL+"),"
                + "CONSTRAINT uPokeballUser UNIQUE (" + ID_USER + "," + ID_POKEBALL+ "));"
                ;
        db.execSQL(CREATE_POKEBALL_INVENTAIRE_TABLE);


    }







    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


    public void addListPokemon(List<Pokemon> pokemonList){

        SQLiteDatabase db =this.getWritableDatabase();
        ContentValues values = new ContentValues();
        for(int i=0;i<(pokemonList.size());i++){
            //values.put(UID_POKEMON,pokemonList.get(i).getOrder());
            values.put(NAME,pokemonList.get(i).getName());
            values.put(HEIGHT,pokemonList.get(i).getHeight());
            values.put(WEIGHT,pokemonList.get(i).getWeight());
            values.put(FRONTRESSOURCE,pokemonList.get(i).getFrontResource());
            //values.put(TYPE1,pokemonList.get(i).getType1String());
            //values.put(TYPE2,pokemonList.get(i).getType2String());
            db.insert(TABLE_POKEMON,null,values);

        }

        db.close();
    }

    public void remplirpokemon(DatabaseHandler db){
        List<Pokemon> pokemonList = new ArrayList<>();
        //Ouverture du fichier res/raw/pokemons
        InputStreamReader isr = new InputStreamReader(this.context.getResources().openRawResource(R.raw.poke));
        BufferedReader reader = new BufferedReader(isr);
        StringBuilder builder = new StringBuilder();
        String data = "";
        //lecture du fichier. data == null => EOF
        while(data != null) {
            try {
                data = reader.readLine();
                builder.append(data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //Traitement du fichier
        try {
            JSONArray array = new JSONArray(builder.toString());
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                String name = object.getString("name");
                String image = object.getString("image");
                int id = this.context.getResources().getIdentifier(image,"drawable",
                        context.getPackageName());
                String type1 = object.getString("type1");
                String type2 = null;
                POKEMON_TYPE type_pokemon_2 = null;
                int order = i + 1;
                if (object.has("type2")) {
                    type2 = object.getString("type2");
                    type_pokemon_2 = POKEMON_TYPE.valueOf(type2);
                }
                double height = object.getDouble("height");
                double weight = object.getDouble("weight");
                Pokemon poke = new Pokemon(order,name, id, POKEMON_TYPE.valueOf(type1), type_pokemon_2,height,weight);
                pokemonList.add(poke);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        addListPokemon(pokemonList);
    }


    @SuppressLint("Range")
    public User getUser() {
        //return first user for now
        //TODO use an id to return the user we want
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_USER, null,null,null,null,null,null);
        User user = new User(1,"marche ap",50);
        if( cursor != null && cursor.moveToFirst() ) {
            user.setId(cursor.getInt(cursor.getColumnIndex(UID)));
            user.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            user.setArgent(cursor.getInt(cursor.getColumnIndex(MONEY)));
        }
        cursor.close();
        db.close();
        return user;

    }

    @SuppressLint("Range")
    public void HealPokemonsUser(int id_user) {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] selectargs = {String.valueOf(id_user)};
        Cursor cursor = db.query(TABLE_USER_POKEMON, null,ID_USER + " =? ",selectargs,null,null,null);
        if( cursor != null && cursor.moveToFirst() ) {
            do {
                String[] selectargs2 = {String.valueOf(cursor.getInt(cursor.getColumnIndex(ID_POKEMON)))};
                Cursor cursorPokemon = db.query(TABLE_POKEMON, null,ID_POKEMON + " =? ",selectargs2,null,null,null);
                healPokemon(id_user,cursor.getInt(cursor.getColumnIndex(ID_POKEMON)), cursorPokemon.getInt(cursorPokemon.getColumnIndex(LIFEMAX)));
                cursorPokemon.close();
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
    }

    private void healPokemon(int id_user, int id_pokemon, int lifeMax){
        SQLiteDatabase db = this.getWritableDatabase();

        String[] selectargs = {String.valueOf(id_user),String.valueOf(id_pokemon)};
        String selection = ID_USER + " =? AND " + ID_POKEMON + " =? ";

        ContentValues values = new ContentValues();
        values.put(NUMBER, lifeMax);

        db.update(TABLE_USER_POKEMON, values, selection, selectargs);
        db.close();
    }

    @SuppressLint("Range")
    public Pokemon getPokemon2() {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_POKEMON, null,null,null,null,null,null);
        Pokemon pokemon = new Pokemon();
        if( cursor != null && cursor.moveToFirst() ) {
            pokemon.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            pokemon.setOrder(cursor.getInt(cursor.getColumnIndex(FRONTRESSOURCE)));

        }
        cursor.close();
        db.close();
        return pokemon;

    }

    @SuppressLint("Range")
    public int getPotionId() {
        //return first potion for now
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_POTION, null,null,null,null,null,null);
        int var = 0;
        if( cursor != null && cursor.moveToFirst() ) {
            var = cursor.getInt(cursor.getColumnIndex(ID_POTION));
        }
        cursor.close();
        db.close();
        return var;

    }

    @SuppressLint("Range")
    public int getNbPotion(int idUser, int idPotion) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = ID_POTION + " =? AND " + ID_USER + " =? ";
        String[] selectargs = {String.valueOf(idPotion),String.valueOf(idUser)};
        Cursor cursor = db.query(TABLE_POTION_INVENTAIRE, null,selection,selectargs,null,null,null);
        cursor.moveToFirst();
        int var = 0;
        if( cursor != null && cursor.moveToFirst() ) {
            var = cursor.getInt(cursor.getColumnIndex(NUMBER));
        }
        cursor.close();
        db.close();
        return var;

    }

    public void updateNbPotion(int idUser, int idPotion, int nbPotion) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(NUMBER, nbPotion);

        String selection = ID_POTION + " =? AND " + ID_USER + " =? ";
        String[] selectargs = {String.valueOf(idPotion),String.valueOf(idUser)};

        int test = db.update(TABLE_POTION_INVENTAIRE, values, selection, selectargs);
        if (test == 0){
            values.put(ID_POTION, idPotion);
            values.put(ID_USER,idUser);
            db.insert(TABLE_POTION_INVENTAIRE, null,values);
        }
        db.close();

    }

    @SuppressLint("Range")
    public int getPokeballId() {
        //return first pokeball for now
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_POKEBALL, null,null,null,null,null,null);
        int var = 0;
        if( cursor != null && cursor.moveToFirst() ) {
            var = cursor.getInt(cursor.getColumnIndex(ID_POKEBALL));
        }
        cursor.close();
        db.close();
        return var;

    }

    @SuppressLint("Range")
    public int getNbPokeball(int idUser, int idPokeball) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = ID_POKEBALL + " =? AND " + ID_USER + " =? ";
        String[] selectargs = {String.valueOf(idPokeball),String.valueOf(idUser)};
        Cursor cursor = db.query(TABLE_POKEBALL_INVENTAIRE, null,selection,selectargs,null,null,null);
        cursor.moveToFirst();
        int var = 0;
        if( cursor != null && cursor.moveToFirst() ) {
            var = cursor.getInt(cursor.getColumnIndex(NUMBER));
        }
        cursor.close();
        db.close();
        return var;

    }

    public void updateNbPokeball(int idUser, int idPokeball, int nbPokeball) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(NUMBER, nbPokeball);

        String selection = ID_POKEBALL + " =? AND " + ID_USER + " =? ";
        String[] selectargs = {String.valueOf(idPokeball),String.valueOf(idUser)};

        int test = db.update(TABLE_POKEBALL_INVENTAIRE, values, selection, selectargs);
        if (test == 0){
            values.put(ID_POKEBALL, idPokeball);
            values.put(ID_USER,idUser);
            db.insert(TABLE_POKEBALL_INVENTAIRE, null,values);
        }
        db.close();

    }

    public void addUser(User user){

        SQLiteDatabase db =this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(UID,user.getId());
        values.put(NAME,user.getName());
        values.put(MONEY,user.getArgent());

        db.insert(TABLE_USER,null,values);
        db.close();
    }

    public void addPotion(int id, String name){

        SQLiteDatabase db =this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ID_POTION,id);
        values.put(NAMEPOTION,name);

        db.insert(TABLE_POTION,null,values);
        db.close();
    }

    public void addPokeball(int id, String name){

        SQLiteDatabase db =this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ID_POKEBALL,id);
        values.put(NAMEPOKEBALL,name);

        db.insert(TABLE_POKEBALL,null,values);
        db.close();
    }

    public void addPokemon(Pokemon pokemon){

        SQLiteDatabase db =this.getWritableDatabase();
        ContentValues values = new ContentValues();
        //values.put(UID_POKEMON,pokemon.getOrder());
        values.put(NAME,pokemon.getName());
        //values.put(HEIGHT,pokemon.getHeight());
        //values.put(WEIGHT,pokemon.getWeight());
        //values.put(FRONTRESSOURCE,pokemon.getFrontResource());

        db.insert(TABLE_POKEMON,null,values);
        db.close();
    }

    public int getPokemon() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_POKEMON, null,null,null,null,null,null);

        // return count
        int var = cursor.getCount();
        cursor.close();
        db.close();
        return var;

    }

    @SuppressLint("Range")
    public List<InterfaceObject> getAllPotion(int id_user){
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = ID_USER + " =? ";
        String[] selectargs = {String.valueOf(id_user)};
        Cursor cursor = db.query(TABLE_POTION_INVENTAIRE, null,selection,selectargs,null,null,null);
        cursor.moveToFirst();
        List<InterfaceObject> listPotion = new ArrayList<>();
        if( cursor != null && cursor.moveToFirst() ) {
            do {
                String[] selectargs2 = {String.valueOf(cursor.getInt(cursor.getColumnIndex(ID_POTION)))};
                Cursor cursorPotion = db.query(TABLE_POTION, null, ID_POTION + " =? ", selectargs2, null, null, null);
                if (cursorPotion != null && cursorPotion.moveToFirst()) {
                    listPotion.add(new Potion(cursorPotion.getString(cursorPotion.getColumnIndex(NAMEPOTION)), cursor.getInt(cursor.getColumnIndex(NUMBER))));
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listPotion;
    }

    @SuppressLint("Range")
    public List<InterfaceObject> getAllPokeball(int id_user){
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = ID_USER + " =? ";
        String[] selectargs = {String.valueOf(id_user)};
        Cursor cursor = db.query(TABLE_POKEBALL_INVENTAIRE, null,selection,selectargs,null,null,null);
        cursor.moveToFirst();
        List<InterfaceObject> listPokeball = new ArrayList<>();
        if( cursor != null && cursor.moveToFirst() ) {
            do {
                String[] selectargs2 = {String.valueOf(cursor.getInt(cursor.getColumnIndex(ID_POKEBALL)))};
                Cursor cursorPokeball = db.query(TABLE_POKEBALL, null, ID_POKEBALL + " =? ", selectargs2, null, null, null);
                if (cursorPokeball != null && cursorPokeball.moveToFirst()) {
                    listPokeball.add(new Pokeball(cursorPokeball.getString(cursorPokeball.getColumnIndex(NAMEPOKEBALL)), cursor.getInt(cursor.getColumnIndex(NUMBER))));
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return listPokeball;
    }

    public void creatuserwithpokemon(User user, Pokemon pokemon) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(LIFE ,pokemon.getHP());

        String selection = ID_POKEMON + " =? AND " + ID_USER + " =? ";
        String[] selectargs = {String.valueOf(pokemon.getOrder()),String.valueOf(user.getId())};

        int test = db.update(TABLE_USER_POKEMON, values, selection, selectargs);
        if (test == 0){
            values.put(ID_USER ,user.getId());
            values.put(ID_POKEMON ,pokemon.getOrder());
            db.insert(TABLE_USER_POKEMON,null,values);

        }
        db.close();

    }
    @SuppressLint("Range")
    public List<Pokemon> getAllPokemonUser(int id_user){
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = ID_USER + " =? ";
        String[] selectargs = {String.valueOf(id_user)};
        Cursor cursor = db.query(TABLE_USER_POKEMON, null,selection,selectargs,null,null,null);
        cursor.moveToFirst();
        List<Pokemon> listPokemon = new ArrayList<>();
        if( cursor != null && cursor.moveToFirst() ) {
            do {
                Log.d("test1","#"+cursor.getInt(cursor.getColumnIndex(LIFE)));
                String[] selectargs2 = {String.valueOf(cursor.getInt(cursor.getColumnIndex(ID_POKEMON)))};
                Log.d("wolajetest",String.valueOf(cursor.getInt(cursor.getColumnIndex(ID_POKEMON))));
                Cursor cursorPokemon = db.query(TABLE_POKEMON, null, UID_POKEMON+ " =? ", selectargs2, null, null, null);
                if (cursorPokemon != null && cursorPokemon.moveToFirst()) {
                    Log.d("test2","#"+cursorPokemon.getDouble(cursorPokemon.getColumnIndex(HEIGHT)));
                    //listPokemon.add(new Pokemon());

                    listPokemon.add(new Pokemon(cursorPokemon.getInt(cursorPokemon.getColumnIndex(UID_POKEMON)),cursorPokemon.getString(cursorPokemon.getColumnIndex(NAME)),cursorPokemon.getInt(cursorPokemon.getColumnIndex(FRONTRESSOURCE)),
                          POKEMON_TYPE.Plante,null,cursorPokemon.getDouble(cursorPokemon.getColumnIndex(HEIGHT)),cursorPokemon.getDouble(cursorPokemon.getColumnIndex(WEIGHT))));
                }
            } while (cursor.moveToNext());
        }
        listPokemon.add(new Pokemon());
        listPokemon.add(new Pokemon());
        cursor.close();
        db.close();
        Log.d("test5","#"+ listPokemon.size());
        return listPokemon;
    }



}