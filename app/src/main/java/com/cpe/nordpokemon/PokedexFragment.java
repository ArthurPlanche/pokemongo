package com.cpe.nordpokemon;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.cpe.nordpokemon.databinding.PokedexFragmentBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class PokedexFragment extends Fragment {
    private OnClickOnNoteListener listener;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        PokedexFragmentBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.pokedex_fragment,container,false);
        binding.pokemonList.setLayoutManager(new LinearLayoutManager(
                binding.getRoot().getContext()));

        List<Pokemon> pokemonList = new ArrayList<>();
        //Ouverture du fichier res/raw/pokemons
        InputStreamReader isr = new InputStreamReader(getResources().openRawResource(R.raw.poke));
        // Ouverture du fichier dans assets
        // InputStreamReader isr =
        // new InputStreamReader(getResources().getAssets().open("pokemons.json"));
        BufferedReader reader = new BufferedReader(isr);
        StringBuilder builder = new StringBuilder();
        String data = "";
        //lecture du fichier. data == null => EOF
        while(data != null) {
            try {
                data = reader.readLine();
                builder.append(data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        //Traitement du fichier
        try {
            JSONArray array = new JSONArray(builder.toString());
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                String name = object.getString("name");
                String image = object.getString("image");
                int id = getResources().getIdentifier(image,"drawable",
                        binding.getRoot().getContext().getPackageName());
                String type1 = object.getString("type1");
                String type2 = null;
                POKEMON_TYPE type_pokemon_2 = null;
                int order = i + 1;
                if (object.has("type2")) {
                    type2 = object.getString("type2");
                    type_pokemon_2 = POKEMON_TYPE.valueOf(type2);
                }
                double height = object.getDouble("height");
                double weight = object.getDouble("weight");
                Pokemon poke = new Pokemon(order,name, id, POKEMON_TYPE.valueOf(type1), type_pokemon_2,height,weight);
                pokemonList.add(poke);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        PokemonListAdapter adapter = new PokemonListAdapter(pokemonList);
        adapter.setOnClickOnNoteListener(this.listener);
        binding.pokemonList.setAdapter(adapter);

        return binding.getRoot();
    }

    public void setOnClickOnNoteListener(OnClickOnNoteListener listener)
    {
        this.listener = listener;
    }

}
