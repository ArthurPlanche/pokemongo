package com.cpe.nordpokemon;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.views.MapView;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class MyThread extends Thread {
    private MyThreadEventListener listener;
    private double south;
    private double west;
    private double north;
    private double east;

    public MyThread(MyThreadEventListener listener, double south, double west, double north, double east) {
        this.south = south;
        this.west = west;
        this.north = north;
        this.east = east;
        this.listener = listener;
    }

    public void run() {
            URL url = null;
            String query = "[out:json];node[amenity=pharmacy]("
                +south
                +","+west+","
                +north+","
                +east+");out;";
            try {
                url = new URL("https://overpass-api.de/api/interpreter?data="+URLEncoder.encode(query, "utf-8"));
                //URL test:
                //url = new URL("https://overpass-api.de/api/interpreter?data=%5Bout%3Ajson%5D%3Bnode%20%5Bamenity%3Dpharmacy%5D%20%2845.76000839079471%2C4.856901168823242%2C45.790659088204684%2C4.902176856994628%29%3B%20out%3B");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        HttpURLConnection c;
            try {
                c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.setRequestProperty("Accept", "*/*");
                c = (HttpURLConnection) url.openConnection();

                /*DataOutputStream printout = new DataOutputStream(c.getOutputStream());
                printout.writeBytes("data=" + URLEncoder.encode(query, "utf-8"));
                printout.flush();
                printout.close();*/

                InputStream istream = c.getInputStream();
                InputStreamReader isr = new InputStreamReader(istream);
                BufferedReader br = new BufferedReader(isr);
                StringBuilder builder = new StringBuilder();

                String data = "";

                do{
                    try {
                        data = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } while(data.contains("elements") != true);

                data = br.readLine();
                data = "[" + data;

                do{
                    try {
                        builder.append(data);
                        data = br.readLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } while(data != null);

                try {
                    JSONArray array = new JSONArray(builder.toString());
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        listener.onEventInMyThread(object);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                istream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
}
