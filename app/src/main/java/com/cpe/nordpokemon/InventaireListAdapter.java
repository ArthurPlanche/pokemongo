package com.cpe.nordpokemon;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.cpe.nordpokemon.databinding.InventaireItemBinding;
import com.cpe.nordpokemon.databinding.PokemonItemBinding;

import java.util.List;

public class InventaireListAdapter extends
        RecyclerView.Adapter<InventaireListAdapter.ViewHolder> {
    List<InterfaceObject> objectList;
    public InventaireListAdapter(List<InterfaceObject> objectList) {
        assert objectList != null;
        this.objectList = objectList;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        InventaireItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.inventaire_item, parent, false);
        return new ViewHolder(binding);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        InterfaceObject object = objectList.get(position);
        holder.viewModel.setObject(object);
    }
    @Override
    public int getItemCount() {
        return objectList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private InventaireItemBinding binding;
        private InventaireViewModel viewModel = new InventaireViewModel();
        ViewHolder(InventaireItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.setInventaireViewModel(viewModel);
        }
    }

}
