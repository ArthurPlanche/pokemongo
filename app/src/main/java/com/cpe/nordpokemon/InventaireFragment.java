package com.cpe.nordpokemon;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.cpe.nordpokemon.databinding.InventaireFragmentBinding;
import com.cpe.nordpokemon.databinding.PokedexFragmentBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class InventaireFragment extends Fragment {

    private DatabaseHandler db;
    public InventaireFragment(DatabaseHandler db) {
        this.db = db;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        InventaireFragmentBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.inventaire_fragment, container, false);
        binding.inventaireList.setLayoutManager(new LinearLayoutManager(
                binding.getRoot().getContext()));

        List<InterfaceObject> objectList;
        User user = db.getUser();
        objectList = db.getAllPotion(user.getId());
        objectList.addAll(db.getAllPokeball(user.getId()));

        InventaireListAdapter adapter = new InventaireListAdapter(objectList);
        binding.inventaireList.setAdapter(adapter);
        return binding.getRoot();
    }
}

