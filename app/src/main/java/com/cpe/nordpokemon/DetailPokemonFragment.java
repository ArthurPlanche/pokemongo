package com.cpe.nordpokemon;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.cpe.nordpokemon.databinding.DetailPokemonFragmentBinding;
import com.cpe.nordpokemon.databinding.PokedexFragmentBinding;

public class DetailPokemonFragment extends Fragment {

    private Pokemon pokemon;

    public DetailPokemonFragment(Pokemon pokemon){this.pokemon = pokemon;}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
                DetailPokemonFragmentBinding binding  = DataBindingUtil.inflate(inflater,
                R.layout.detail_pokemon_fragment,container,false);
                PokemonViewModel viewModel = new PokemonViewModel();
                viewModel.setPokemon(pokemon);
                binding.setPokemonViewModel(viewModel);






        return binding.getRoot();
    }
}
