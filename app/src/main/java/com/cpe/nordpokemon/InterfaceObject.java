package com.cpe.nordpokemon;

public interface InterfaceObject {
    String getName();
    int getNumber();
}
