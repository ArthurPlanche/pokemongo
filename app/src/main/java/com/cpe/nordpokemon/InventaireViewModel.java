package com.cpe.nordpokemon;

        import androidx.databinding.BaseObservable;
        import androidx.databinding.Bindable;

public class InventaireViewModel extends BaseObservable {
    private InterfaceObject Object;

    @Bindable
    public String getName() {
        return Object.getName();
    }

    @Bindable
    public String getNumber() {
        return "Nb: " + Object.getNumber();
    }

    public void setObject(InterfaceObject object){
        this.Object = object;
        notifyChange();
    }

}
